import { allSettled, fork } from "effector";
import {
  $cart,
  $cartIsEmpty,
  $menu,
  $subTotalDollars,
  $totalDollars,
  $totalTaxDollars,
  addItem,
  getInitialState,
  removeItem,
} from "../state";

describe("Cart | Store", () => {
  it("should add an item into the cart", async () => {
    const scope = fork({
      values: new Map().set($menu, getInitialState()),
    });

    await allSettled(addItem, {
      scope,
      params: "Chicken Salad with Parmesan",
    });

    expect(scope.getState($cart).length).toBe(1);

    await allSettled(addItem, {
      scope,
      params: "Salmon and Vegetables",
    });

    expect(scope.getState($cart).length).toBe(2);
  });

  it("should set cartIsEmpty to true when there is no items in it", async () => {
    const scope = fork({
      values: new Map().set($menu, getInitialState()),
    });

    await allSettled(addItem, {
      scope,
      params: "Chicken Salad with Parmesan",
    });

    expect(scope.getState($cart).length).toBe(1);

    await allSettled(removeItem, {
      scope,
      params: "Chicken Salad with Parmesan",
    });
    expect(scope.getState($cart).length).toBe(0);
    expect(scope.getState($cartIsEmpty)).toBeTruthy();
  });

  it("should count the cart total price", async () => {
    const scope = fork({
      values: new Map().set($menu, getInitialState()),
    });

    await allSettled(addItem, {
      scope,
      params: "Chicken Salad with Parmesan",
    });
    await allSettled(addItem, {
      scope,
      params: "Chicken Salad with Parmesan",
    });
    await allSettled(addItem, {
      scope,
      params: "Bacon, Eggs, and Toast",
    });

    expect(scope.getState($subTotalDollars)).toBe("$19.95");
    expect(scope.getState($totalTaxDollars)).toBe("$1.94");
    expect(scope.getState($totalDollars)).toBe("$21.89");
  });

  it("should increase and decrease the quantity in the cart", async () => {
    const scope = fork({
      values: new Map().set($menu, getInitialState()),
    });

    await allSettled(addItem, {
      scope,
      params: "Chicken Salad with Parmesan",
    });
    await allSettled(addItem, {
      scope,
      params: "Salmon and Vegetables",
    });

    expect(
      scope.getState($cart).reduce((acc, item) => acc + item.count, 0)
    ).toBe(2);

    await allSettled(removeItem, {
      scope,
      params: "Chicken Salad with Parmesan",
    });
    await allSettled(removeItem, {
      scope,
      params: "Salmon and Vegetables",
    });

    expect(
      scope.getState($cart).reduce((acc, item) => acc + item.count, 0)
    ).toBe(0);
  });

  it("should prevent decrease item count below 0", async () => {
    const scope = fork({
      values: new Map().set($menu, getInitialState()),
    });

    expect(
      scope
        .getState($menu)
        .find((item) => item.name === "Chicken Salad with Parmesan")!.count
    ).toBe(0);

    await allSettled(removeItem, {
      scope,
      params: "Chicken Salad with Parmesan",
    });

    expect(
      scope
        .getState($menu)
        .find((item) => item.name === "Chicken Salad with Parmesan")!.count
    ).toBe(0);
  });
});
