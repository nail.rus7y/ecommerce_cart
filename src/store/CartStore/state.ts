import { combine, createDomain, sample } from "effector";
import { MenuItem, menuItems } from "../../static/menuItems";

const taxRatio = 0.0975;

export const getInitialState = () => [
  ...menuItems.map((item) => ({ ...item })),
];
export const eCommerceCartDomain = createDomain();

export const addItem = eCommerceCartDomain.createEvent<MenuItem["name"]>();
export const removeItem = eCommerceCartDomain.createEvent<MenuItem["name"]>();

export const $menu = eCommerceCartDomain
  .createStore(getInitialState())
  .on(addItem, (state, name) =>
    state.map((item) => {
      if (item.name === name) item.count++;
      return { ...item };
    })
  )
  .on(removeItem, (state, name) =>
    state.map((item) => {
      if (item.name === name && item.count !== 0) item.count--;
      return { ...item };
    })
  );

export const $cart = eCommerceCartDomain.createStore<MenuItem[]>([]);

sample({
  clock: [addItem, removeItem],
  source: $menu,
  fn: (state) => state.filter((item) => item.count),
  target: $cart,
});

export const $cartIsEmpty = $cart.map((state) => !state.length);

export const $subTotalCents = $cart.map((state) =>
  state.reduce((acc, item) => acc + item.price * item.count, 0)
);
export const $totalTax = $subTotalCents.map((state) =>
  Math.floor(state * taxRatio)
);
export const $totalCents = combine(
  $subTotalCents,
  $totalTax,
  (cents, tax) => cents + tax
);

export const $subTotalDollars = $subTotalCents.map(
  (state) => `$${Math.floor(state / 100)}.${state % 100}`
);
export const $totalTaxDollars = $totalTax.map(
  (state) => `$${Math.floor(state / 100)}.${state % 100}`
);
export const $totalDollars = $totalCents.map(
  (state) => `$${Math.floor(state / 100)}.${state % 100}`
);
