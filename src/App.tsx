import React from "react";
import "./styles/styles.scss";
import { ItemsPanel } from "./modules/ItemsPanel";
import { CartPanel } from "./modules/CartPanel";

export default function App() {
  return (
    <div className="wrapper menu">
      <ItemsPanel />
      <CartPanel />
    </div>
  );
}
