import React from "react";
import { Totals } from "./components/patterns/Totals";
import { CartSummary } from "./components/patterns/CartSummary";

export default function CartPanel() {
  return (
    <div className="panel cart">
      <h1>Your Cart</h1>
      <CartSummary />
      <Totals />
    </div>
  );
}
