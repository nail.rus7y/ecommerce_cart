import React from "react";
import { MenuItem } from "../../../../../static/menuItems";
import { addItem, removeItem } from "../../../../../store/CartStore/state";

export default function CartItem({ count, alt, name, price, image }: MenuItem) {
  const onAddItem = () => addItem(name);
  const onRemoveItem = () => removeItem(name);

  return (
    <li>
      <div className="plate">
        <img src={require(`Images/${image}`)} alt={alt} className="plate" />
        <div className="quantity">{count}</div>
      </div>
      <div className="content">
        <p className="menu-item">{name}</p>
        <p className="price">{`${Math.floor(price / 100)}.${price % 100}`}</p>
      </div>
      <div className="quantity__wrapper">
        <button className="decrease" onClick={onRemoveItem}>
          <img src={require("Images/chevron.svg")} alt={"decrease button"} />
        </button>
        <div className="quantity">{count}</div>
        <button className="increase" onClick={onAddItem}>
          <img src={require("Images/chevron.svg")} alt={"increase button"} />
        </button>
      </div>
      <div className="subtotal">{`${Math.floor((price * count) / 100)}.${
        (price * count) % 100
      }`}</div>
    </li>
  );
}
