import React from "react";
import { useStore } from "effector-react";
import {
  $subTotalDollars,
  $totalDollars,
  $totalTaxDollars,
} from "../../../../../store/CartStore/state";

export default function Totals() {
  const subTotalDollars = useStore($subTotalDollars);
  const totalTaxDollars = useStore($totalTaxDollars);
  const totalDollars = useStore($totalDollars);

  return (
    <div className="totals">
      <div className="line-item">
        <div className="label">Subtotal:</div>
        <div className="amount price subtotal">{subTotalDollars}</div>
      </div>
      <div className="line-item">
        <div className="label">Tax:</div>
        <div className="amount price tax">{totalTaxDollars}</div>
      </div>
      <div className="line-item total">
        <div className="label">Total:</div>
        <div className="amount price total">{totalDollars}</div>
      </div>
    </div>
  );
}
