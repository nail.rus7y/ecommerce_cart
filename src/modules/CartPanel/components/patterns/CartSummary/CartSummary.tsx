import React from "react";
import { CartItem } from "../../atoms/CartItem";
import { useList, useStore } from "effector-react";
import { $cart, $cartIsEmpty } from "../../../../../store/CartStore/state";

export default function CartSummary() {
  const cartIsEmpty = useStore($cartIsEmpty);
  const cart = useList($cart, (item) => <CartItem key={item.name} {...item} />);

  return cartIsEmpty ? (
    <p className="empty">Your cart is empty.</p>
  ) : (
    <ul className="cart-summary">{cart}</ul>
  );
}
