import React from "react";
import { ItemsList } from "./components/patterns/ItemsList";

export default function ItemsPanel() {
  return (
    <div className="panel">
      <h1>To Go Menu</h1>
      <ItemsList />
    </div>
  );
}
