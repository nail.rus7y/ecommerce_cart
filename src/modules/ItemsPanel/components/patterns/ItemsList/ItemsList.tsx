import React from "react";
import { Item } from "../../atoms/Item";
import { useList } from "effector-react";
import { $menu } from "../../../../../store/CartStore/state";

export default function ItemsList() {
  const items = useList($menu, (item) => <Item key={item.name} {...item} />);

  return <ul className="menu">{items}</ul>;
}
