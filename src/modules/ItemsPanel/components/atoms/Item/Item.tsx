import React from "react";
import { MenuItem } from "../../../../../static/menuItems";
import { addItem } from "../../../../../store/CartStore/state";

export default function Item({ name, count, image, price, alt }: MenuItem) {
  const onAddItem = () => addItem(name);

  return (
    <li>
      <div className="plate">
        <img
          src={require(`../../../../../images/${image}`)}
          alt={alt}
          className="plate"
        />
      </div>
      <div className="content">
        <p className="menu-item">{name}</p>
        <p className="price">{`${Math.floor(price / 100)}.${price % 100}`}</p>
        {count ? (
          <button className="in-cart">
            <img src={require("Images/check.svg")} alt="Check" />
            In Cart
          </button>
        ) : (
          <button className="add" onClick={onAddItem}>
            Add to Cart
          </button>
        )}
      </div>
    </li>
  );
}
