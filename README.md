# eCommerce_cart

### What?
It's a simple implementation of eCommerce cart component.

### List if features
- [x] View the plates on the left side of the screen and add them to your cart on the right side.
- [x] When there are no plates within your cart, you should see a message that says, "Your cart is empty."
- [x] When a plate is added to your cart, the Subtotal and Totals will automatically update.
- [x] When products are in your cart, you should be able to increase and decrease the quantity.
- [x] A user should not be able to mark the quantity as a negative number. 
- [x] If the quantity goes down to 0, the user will have the option to delete or remove the product from their cart entirely.
- [x] Tax is based on the state of Tennessee sales tax: 0.0975

### Used libraries
- Webpack
- React
- EffectorJS
- Babel
- Cypress
- Typescript
- Prettier
- Eslint
