const plugins = ["effector/babel-plugin"];

if (process.env.NODE_ENV === "development") {
  plugins.push([
    "effector-logger/babel-plugin",
    { inspector: true, effector: { addLoc: true } },
  ]);
  plugins.push("react-refresh/babel");
}

module.exports = {
  presets: [
    "@babel/preset-env",
    "@babel/preset-react",
    "@babel/preset-typescript",
  ],
  plugins,
};
